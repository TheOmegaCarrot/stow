# _________  _   _
#|__  / ___|| | | |
#  / /\___ \| |_| |
# / /_ ___) |  _  |
#/____|____/|_| |_|
#
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/${USER}/.oh-my-zsh"

DATE=$(date +"%y-%m-%d")

typeset -U path path=(/opt/cmake_distributions/latest/bin ${HOME}/.cabal/bin ${HOME}/.ghcup/bin /opt/llvm-latest/bin ${HOME}/.cargo/bin ${HOME}/scripts ${HOME}/.local/bin /opt/bin $path);

export CDPATH=".:${HOME}/bookmarks"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="false"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="false"

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="false"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
DISABLE_LS_COLORS="false"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  # zsh-syntax-highlighting
  fast-syntax-highlighting
  zsh-autosuggestions
  zsh-vi-mode
  #globalias
  sudo
  command-not-found
  # zsh-interactive-cd
  fzf
  # git-auto-fetch
)

source $ZSH/oh-my-zsh.sh

ZSH_ALIAS_FINDER_AUTOMATIC=true

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
export EDITOR='nvim'
# export PAGER="nvim +'set ft=man' +'set number relativenumber' +'set scrolloff=999' +'norm M'"
export PAGER="bat --color=always"
export MANROFFOPT='-c'
export MANPAGER="sh -c 'col -bx | bat -l man -p --color=always'"
export GIT_PAGER="bat --color=always"

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# Settings
setopt auto_cd
setopt auto_pushd
# setopt CHASE_LINKS
setopt AUTO_CONTINUE

# = expansion
# example: =ls equivalent to $(whence ls)
set EQUALS
setopt EXTENDED_GLOB

# setopt correct_all
# export SPROMPT="Correct $fg[red]%R$reset_color to $fg[green]%r$reset_color?
#  [Yes, No, Abort, Edit] "

# _comp_options+=(globdots)

setopt auto_list
setopt auto_menu

autoload -U zcalc

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Initialize
# if [ `tput colors` = "256" ]; then
# colorscript random
clear
pfetch
# fi

alias x="exit"
alias please="sudo"
alias neo="neofetch"
alias up="uptime"
alias convim="nvim ~/.config/nvim/init.vim"
alias conz="nvim ~/.zshrc"
alias conalac="nvim ~/.config/alacritty/alacritty.yml"
alias conkitty="nvim ~/.config/kitty/kitty.conf"
alias consxh="nvim ~/.config/sxhkd/sxhkdrc"
alias conbsp="nvim ~/.config/bspwm/bspwmrc"
alias conhypr="nvim ~/.config/hypr/hyprland.conf"
# alias rl="source ~/.zshrc"
alias rl="exec zsh"
# alias ls="ls --color=auto --group-directories-first"
alias ls="exa --icons --group-directories-first -I='.git'"
alias l="exa --icons --header --group-directories-first --group --long --all -I='.git'"
alias lt="exa --all --tree --icons -I='.git'"
alias la="exa --all --icons -I='.git'"
alias exa="exa --icons --group-directories-first -I='.git'"
alias exl="exa --icons --group-directories-first --long --all -I='.git'"
alias et="exa --tree -I='.git'"
alias eta="exa --all --tree -I='.git'"
alias sl="sl -e"
alias mv="mv -v"
alias cp="cp -vr"
alias cpl="cp -vrL"
alias ncdu="ncdu --color dark"
# alias rs="rsync -aHAXvhP" # -rlptgoDHAXvhP
alias rs="rsync -rlptgoDHAXvhP"
alias mkd="mkdir -pv"
alias mkdir="mkdir -pv"
alias rmdir="rmdir -v"
alias v="nvim"
# alias aptup="sudo apt update && apt list --upgradable && sudo apt upgrade -y && flatpak update -y"
# alias aptc="sudo apt autoclean && sudo apt autoremove -y --purge && flatpak uninstall --unused -y"
# alias aptupc="sudo apt update && apt list --upgradable && sudo apt upgrade -y && flatpak update -y && sudo apt autoclean && sudo apt autoremove -y --purge && flatpak uninstall --unused -y"
alias ydl='yt-dlp -f bestvideo+bestaudio -ciw --audio-quality 0 --retries infinite -v --restrict-filenames --output "%(uploader)s_-_%(title)s.%(ext)s"'
alias yl='yt-dlp --list-formats --no-playlist'
# alias c="clear && colorscript random"
alias c="clear && pfetch"
alias pg="ps -e | grep -i"
alias beep='echo -e "\07"'
alias sxiv="sxiv -a"
alias py="python3"
alias mk='make -j $(nproc)'
alias mkt='make -j $(nproc) all test'
alias mc="make clean"
alias nk='ninja'
alias nc="ninja clean"
alias cmk="cmake --build . --parallel $(nproc)"
alias cmc="cmake --build . --target clean"
alias cmkt="cmake --build . --parallel $(nproc) && cmake --build . --parallel $(nproc) --target test"
# alias nt="bspc rule -a kitty -o state=floating center=true rectangle=600x500+0+0 && kitty 2>/dev/null & disown"
alias shac="shasum -c *.sha*sum"
# alias matm="matlab -nodesktop -nosplash <"
# alias matterm="matlab -nodesktop -nosplash"
alias udm="udiskie-mount"
alias udu="udiskie-umount"
alias lb="lsblk -f"
alias dv="dirs -v"
alias dush="du -sh *"
alias df="df -h"
alias xzz="xz -kzvv -T 0"
alias zstz="zstd -v --compress --keep --threads=$(nproc)"
alias sx='startx'
alias hypr='Hyprland'
alias vtex='nvim *.tex'
alias lmk='latexmk -pdflatex'
alias syncthing-console='firefox localhost:8384 & disown'
alias zat='zathura --fork'

# Exported so that PlantUML functionality
# in the diagram-generator Pandoc Lua filer works
# as expected.
export PLANTUML=/usr/share/java/plantuml/plantuml.jar

# ctest environment variables
export CTEST_PROGRESS_OUTPUT=1
export CTEST_OUTPUT_ON_FAILURE=1
export CTEST_PARALLEL_LEVEL=$(nproc)

# Cmake packages
export CMAKE_PREFIX_PATH="/opt/cmake_packages"

# Ninja formatting :)
export NINJA_STATUS="[%p :: %r/%f/%u :: %t :: %e] "

if pgrep Xorg > /dev/null ; then
  ~/.config/xinputfixer
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

if [ `tput colors` != "256" ]; then
	source ~/.p10k.zsh.tty
fi


# add line numbers and color to output
function number() {
	awk '{print " " $0}' | grep -n .
}

CHPWD_CMD="exa --icons --group-directories-first --all"

# auto-ls
function chpwd() {
  eval -- ${CHPWD_CMD}
}

function cdr() {
  function chpwd() { }

	cd $(realpath .)

  function chpwd() {
    eval -- ${CHPWD_CMD}
  }
}

# simple fzf cd
function cdf() {
	dest=$(find "${1:=.}" -type d | sed "s|${HOME}|~|" | fzf | sed "s|~|${HOME}|")
	[ -z "${dest}" ] || cd "${dest}"
}

# cd to root of git repo
function cdg() {
  cd $(git rev-parse --show-toplevel)
}

# evaluates to fzf result
function pfz() {
  if [ "${1}" = "-f" ]; then
    findflags=-type f
    shift
  fi
	file=$(find "${1:=.}" ${findflags} | sed "s|${HOME}|~|" | fzf | sed "s|~|${HOME}|")
	[ -z "${file}" ] || echo "${file}"
}

# fzf through the filesystem!
function vcd() {

	# No need to pushd every intermediate directory
	unsetopt auto_pushd

	if [ ! -z ${1} ]; then
		cd "${1}"
		shift
	fi

	while true; do

		if [ "${1}" = "-a" ]; then
			foundDirs=$(find -L . -maxdepth 1 -type d | sed 's|\./||g' | grep -v "^\.$")
		else
			foundDirs=$(find -L . -maxdepth 1 -type d -not -path '*/\.*' | sed 's|\./||g' | grep -v "^\.$")
		fi

		# dont bother staying in fzf if there are no more directories
		if [ ! -z ${foundDirs} ]; then
			# add option to go up a directory
			foundDirs=$(echo -e "${foundDirs}\n..")

			target=$(fzf <<< ${foundDirs})
		else
			# prevent infinite loop
			target=''
		fi

		# allow for exit by just hitting esc
		if [ -z ${target} ]; then
			break
		fi

		# cd to selected directory
		cd "${target}"

	done

	# just looks messy with chpwd if you dont clear
	clear

	# these two just for pretty, can be safely removed
	pfetch
	exa --icons --group-directories-first --all

	# Don't leave it off!
	setopt auto_pushd

}

function mkc() {
	if [ -z ${1} ]; then
		echo "mkc: missing operand"
	else
		mkdir -p "${1}"
		cd "${1}"
	fi
}

# function zat() {
#   zathura $@ & disown
# }
