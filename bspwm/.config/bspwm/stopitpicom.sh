#!/bin/bash

bspc subscribe node_remove | while true ; do
  sleep 0.3 ; bspc node -f
done
