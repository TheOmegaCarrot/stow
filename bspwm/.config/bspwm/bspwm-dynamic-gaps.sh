#!/bin/bash
# first argument is "inc" or "dec" for increase or decrease gaps
# second argument is how many pixels the gaps will be adjusted by

# get size of gaps before doing anything
GAPSPRE=$(cat /tmp/bspwm-gaps.txt)
DELTA=$2

# if no argument, return error
if [[ $1 == "" ]]
then
	exit 1
fi

# if argument is inc
if [[ $1 == "inc" ]]
then
	GAPSPOST=$(expr ${GAPSPRE} + ${DELTA})
fi

# if argument is dec
if [[ $1 == "dec" ]]
then
	GAPSPOST=$(expr ${GAPSPRE} - ${DELTA})
fi

echo ${GAPSPOST} > /tmp/bspwm-gaps.txt

bspc config window_gap ${GAPSPOST}
