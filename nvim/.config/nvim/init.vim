"__     _____ __  __ ____   ____
"\ \   / /_ _|  \/  |  _ \ / ___|
" \ \ / / | || |\/| | |_) | |
"  \ V /  | || |  | |  _ <| |___
"   \_/  |___|_|  |_|_| \_\\____|

" I never want EX mode
nnoremap Q <nop>
nnoremap gQ <nop>

let mapleader = " "

"Plugins!!
call plug#begin('~/.vim/plugged')

Plug 'machakann/vim-highlightedyank'
Plug 'Kjwon15/vim-transparent'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-repeat'
Plug 'machakann/vim-swap'
Plug 'jamessan/vim-gnupg'
" Plug 'ervandew/supertab'
Plug 'tpope/vim-commentary'
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'szw/vim-maximizer'
Plug 'voldikss/vim-floaterm'
Plug 'chrisbra/unicode.vim'
Plug 'airblade/vim-gitgutter'
Plug 'phaazon/hop.nvim'
Plug 'skywind3000/asyncrun.vim'
Plug 'kana/vim-niceblock'
Plug 'tommcdo/vim-exchange'
Plug 'bkad/CamelCaseMotion'
Plug 'tpope/vim-unimpaired'
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
Plug 'junegunn/vim-easy-align'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'nvim-lualine/lualine.nvim'
Plug 'akinsho/bufferline.nvim'
Plug 'kana/vim-operator-user'
Plug 'rhysd/vim-clang-format'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'windwp/nvim-autopairs'
Plug 'dhruvasagar/vim-table-mode'
Plug 'aklt/plantuml-syntax'
Plug 'KeitaNakamura/tex-conceal.vim'
Plug 'stevearc/dressing.nvim'
Plug 'f-person/git-blame.nvim'
Plug 'rebelot/kanagawa.nvim'
Plug 'mattn/emmet-vim'
Plug 'zef/vim-cycle'
Plug 'glts/vim-magnum'
Plug 'glts/vim-radical'
" Plug 'krivahtoo/silicon.nvim', { 'do': './install.sh build' }
Plug 'michaelrommel/nvim-silicon'


" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate'}
Plug 'https://gitlab.com/HiPhish/rainbow-delimiters.nvim'
" Plug 'p00f/nvim-ts-rainbow'
Plug 'nvim-treesitter/nvim-treesitter-refactor'
Plug 'Mofiqul/dracula.nvim'
Plug 'kylechui/nvim-surround'
Plug 'neovim/nvim-lspconfig'
Plug 'p00f/clangd_extensions.nvim'
Plug 'mfussenegger/nvim-jdtls'
Plug 'MysticalDevil/inlay-hints.nvim'

Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'


" Bonus text objects!
Plug 'kana/vim-textobj-user'
Plug 'vim-scripts/argtextobj.vim'
Plug 'somini/vim-textobj-fold'
Plug 'glts/vim-textobj-comment'
Plug 'kana/vim-textobj-line'
Plug 'Julian/vim-textobj-variable-segment'

" Dumb stuff
Plug 'tamton-aquib/duck.nvim'
Plug 'seandewar/killersheep.nvim'

call plug#end()


" Duck nonsense
nnoremap <leader>\kh :lua require("duck").hatch()<CR>
nnoremap <leader>\kc :lua require("duck").cook()<CR>

lua << EOF

math.randomseed(os.time())
if (math.random(1, 10)) == 1 then
  require("duck").hatch()
end

EOF

" vim-hexokinase
let g:Hexokinase_highlighters = ['virtual']
" let g:Hexokinase_highlighters = ['backgroundfull']

" Set color scheme
set termguicolors
set background=dark

lua << EOF

require('kanagawa').setup({
  -- theme = "wave"
  -- theme = "dragon"
  theme = "wave",
})

EOF

" colorscheme dracula
colorscheme kanagawa

" fzf.vim
nnoremap <leader>o :Files<CR>
nnoremap <leader>gf :GFiles<CR>
nnoremap <leader>gc :GFiles?<CR>
nnoremap <leader><leader>h :Files<space>~<CR>
nnoremap <leader>gt :Tags<CR>
nnoremap <leader>b :Buffers<CR>
nnoremap <leader>lc :BLines<CR>
nnoremap <leader>lb :Lines<CR>

" vim-maximizer
nnoremap <leader>m :MaximizerToggle!<CR>

" vim-floaterm
nnoremap <A-,> :FloatermNew --width=0.8 --height=0.8 --autoclose=1<CR>

" unicode.vim
nnoremap <leader>us :UnicodeSearch!<space>
inoremap ;us <esc>:UnicodeSearch!<space>
" Clobbered by junegunn/vim-easy-align
" Haven't figured out a new mapping yet
" nnoremap ga :UnicodeName<CR>

" vim-gitgutter
" autocmd VimEnter * GitGutterLineHighlightsEnable
" autocmd VimEnter * GitGutterLineNrHighlightsEnable
highlight GitGutterAdd guifg=#50fa7b ctermfg=2
let g:gitgutter_map_keys = 0
nmap ]h <Plug>(GitGutterNextHunk)
nmap [h <Plug>(GitGutterPrevHunk)
omap ih <Plug>(GitGutterTextObjectInnerPending)
omap ah <Plug>(GitGutterTextObjectOuterPending)
xmap ih <Plug>(GitGutterTextObjectInnerVisual)
xmap ah <Plug>(GitGutterTextObjectOuterVisual)
nnoremap <leader>gu :GitGutterUndoHunk<CR>

" hop.nvim
lua require'hop'.setup()
nmap Q <cmd>HopChar1<CR>
xmap Q <cmd>HopChar1<CR>
nmap gQQ <cmd>HopChar2<CR>
xmap gQQ <cmd>HopChar2<CR>
nmap gQL <cmd>HopLine<CR>
xmap gQL <cmd>HopLine<CR>
nmap gQV <cmd>HopVertical<CR>
xmap gQV <cmd>HopVertical<CR>

" vim-markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_autowrite = 1
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_math = 1
let g:vim_markdown_strikethrough = 1

" vim-easy-align
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" CamelCaseMotion
let g:camelcasemotion_key = ','

" emmet-vim
let g:user_emmet_mode='inv'  "enable all functions
let g:user_emmet_leader_key='<C-,>'
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

" nvim-autopairs
lua << EOF

require('nvim-autopairs').setup({
  disable_filetype = { "TelescopePrompt" },
  disable_in_macro = false,
  disable_in_visualblock = false,
  ignored_next_char = [=[[%w%%%'%[%"%.]]=],
  enable_moveright = true,
  enable_afterquote = true,
  enable_check_bracket_line = true,
  enable_bracket_in_quote = true,
  break_undo = true,
  check_ts = true,
  map_cr = true,
  map_bs = true,
  map_c_h = false,
  map_c_w = false,
})

local Rule = require('nvim-autopairs.rule')
local npairs = require('nvim-autopairs')
local cond = require('nvim-autopairs.conds')

npairs.add_rules({
  Rule("$", "$",{"tex", "latex", "markdown"})
    :with_move(cond.none())
})

npairs.add_rules({
  Rule("\\[", "\\]",{"tex", "latex"})
})

npairs.add_rules({
  Rule("\\(", "\\)",{"tex", "latex"})
})

npairs.add_rules({
  Rule("<", ">",{"cpp"})
    :with_pair(cond.not_after_text(" "))
    :with_move(cond.none())
    :with_cr(cond.done())
})

npairs.add_rules({
  Rule("/*", "*/",{"cpp","java"})
})

local brackets = { { '(', ')' }, { '[', ']' }, { '{', '}' } }
npairs.add_rules {
  Rule(' ', ' ')
    :with_pair(function (opts)
      local pair = opts.line:sub(opts.col - 1, opts.col)
      return vim.tbl_contains({
        brackets[1][1]..brackets[1][2],
        brackets[2][1]..brackets[2][2],
        brackets[3][1]..brackets[3][2],
      }, pair)
    end)
}
for _,bracket in pairs(brackets) do
  npairs.add_rules {
    Rule(bracket[1]..' ', ' '..bracket[2])
      :with_pair(function() return false end)
      :with_move(function(opts)
        return opts.prev_char:match('.%'..bracket[2]) ~= nil
      end)
      :use_key(bracket[2])
  }
end

for _,punct in pairs { ",", ";" } do
    require "nvim-autopairs".add_rules {
        require "nvim-autopairs.rule"("", punct)
            :with_move(function(opts) return opts.char == punct end)
            :with_pair(function() return false end)
            :with_del(function() return false end)
            :with_cr(function() return false end)
            :use_key(punct)
    }
end

EOF

" Clang-Format
autocmd FileType cpp let g:clang_format#auto_format = 1
autocmd FileType cpp nnoremap <leader>f :ClangFormat<CR>
autocmd FileType java nnoremap <leader>f :ClangFormat<CR>

" Indent-blankline
lua << EOF

-- Too slow for my old thinkpads :(

-- local highlight = {
--     "RainbowRed",
--     "RainbowYellow",
--     "RainbowBlue",
--     "RainbowOrange",
--     "RainbowGreen",
--     "RainbowViolet",
--     "RainbowCyan",
-- }
-- local hooks = require "ibl.hooks"
-- -- create the highlight groups in the highlight setup hook, so they are reset
-- -- every time the colorscheme changes
-- hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
--     vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
--     vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
--     vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
--     vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
--     vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
--     vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
--     vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
-- end)
-- 
-- vim.g.rainbow_delimiters = { highlight = highlight }
-- require("ibl").setup { scope = { highlight = highlight } }
-- 
-- hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)

require("ibl").setup()

EOF

" bufferline.nvim
lua << EOF

require('bufferline').setup {
        options = {
            mode = "tabs",
            themable = true,
            numbers = "none",
            close_command = "bdelete! %d",
            right_mouse_command = "bdelete! %d",
            left_mouse_command = "buffer %d",
            middle_mouse_command = nil,
            indicator = {
                icon = '▎',
                style = 'icon'
            },
            buffer_close_icon = '󰅖',
            modified_icon = '●',
            close_icon = '',
            left_trunc_marker = '',
            right_trunc_marker = '',
            name_formatter = function(buf)
            end,
            max_name_length = 18,
            max_prefix_length = 15,
            truncate_names = true,
            tab_size = 18,
            diagnostics = "nvim_lsp",
            diagnostics_update_in_insert = false,
            diagnostics_indicator = function(count, level, diagnostics_dict, context)
                return "("..count..")"
            end,
            custom_filter = function(buf_number, buf_numbers)
                if vim.bo[buf_number].filetype ~= "<i-dont-want-to-see-this>" then
                    return true
                end
                if vim.fn.bufname(buf_number) ~= "<buffer-name-I-dont-want>" then
                    return true
                end
                if vim.fn.getcwd() == "<work-repo>" and vim.bo[buf_number].filetype ~= "wiki" then
                    return true
                end
                if buf_numbers[1] ~= buf_number then
                    return true
                end
            end,
            offsets = {
                {
                    filetype = "NvimTree",
                    text = "File Explorer",
                    text_align = "center",
                    separator = true
                }
            },
            color_icons = true,
            show_buffer_icons = true,
            show_buffer_close_icons = true,
            show_close_icon = false,
            show_tab_indicators = true,
            show_duplicate_prefix = true,
            persist_buffer_sort = true,
            move_wraps_at_ends = false,
            separator_style = "padded_slope",
            enforce_regular_tabs = false,
            always_show_bufferline = false,
            hover = {
                enabled = true,
                delay = 200,
                reveal = {'close'}
            },
            sort_by = 'tabs'
        }
}

EOF

" Treesitter
lua << EOF
require'nvim-treesitter.configs'.setup {
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
	rainbow = {
		enable = true,
		extended_mode = true,
	},
	refactor = {
		highlight_definitions = {
			enable = true,
		},
		-- smart_rename = {
		-- 	enable = true,
		-- 	keymaps = {
		-- 		smart_rename = "grr",
		-- 	},
		-- },
		-- navigation = {
		-- 	enable = true,
		-- 	keymaps = {
		-- 		goto_definition = "gnd",
		-- 		goto_next_usage = "<a-*>",
		-- 		goto_previous_usage = "<a-#>",
		-- 	},
		-- },
	},
}

EOF

set completeopt=menu,menuone,noselect

lua << EOF
  -- Set up nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    window = {
      -- completion = cmp.config.window.bordered(),
      -- documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      -- ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      -- ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<Tab>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
    }, {
      { name = 'buffer' },
    }),
    sorting = {
        comparators = {
        cmp.config.compare.offset,
        cmp.config.compare.exact,
        cmp.config.compare.recently_used,
        require("clangd_extensions.cmp_scores"),
        cmp.config.compare.kind,
        cmp.config.compare.sort_text,
        cmp.config.compare.length,
        cmp.config.compare.order,
        },
    },
  })

local cmp_autopairs = require('nvim-autopairs.completion.cmp')
local cmp = require('cmp')
cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)

EOF

" There's gotta be a better way to do this through treesitter
" Highlight auto like a type in c++
" autocmd FileType cpp match type /auto/

" git-blame.nvim
let g:gitblame_display_virtual_text = 0

" lualine
lua << EOF
local git_blame = require('gitblame')

require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'auto',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {
      statusline = {},
      winbar = {},
    },
    ignore_focus = {},
    always_divide_middle = true,
    globalstatus = false,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    }
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
		lualine_c = {'windows'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
  extensions = {}
}

EOF

lua require("nvim-surround").setup()

" lspconfig
lua << EOF

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
-- vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', '<leader>h', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  -- vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  -- require("clangd_extensions.inlay_hints").setup_autocmd()
  -- require("clangd_extensions.inlay_hints").set_inlay_hints()
end

local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}

require'lspconfig'.rust_analyzer.setup{
  cmd = {"rust-analyzer"},
  filetypes = {"rust"},
  settings = {
        ["rust-analyzer"] = {
            imports = {
                granularity = {
                    group = "module",
                },
                prefix = "self",
            },
            cargo = {
                buildScripts = {
                    enable = true,
                },
            },
            procMacro = {
                enable = true
            },
        }
    }
}

require'lspconfig'.pylsp.setup{
  cmd = {"pylsp"},
  filetypes = {"python"},
  single_file_support = true,
  settings = {
    pylsp = {
      plugins = {
        pycodestyle = {
          enabled = true,
          maxLineLength = 100
        }
      }
    }
  }
}

require'lspconfig'.hls.setup{
  filetypes = { 'haskell', 'lhaskell', 'cabal' },
  cmd = { "haskell-language-server-wrapper", "--lsp" },
  haskell = {
    cabalFormattingProvider = "cabalfmt",
    formattingProvider = "ormolu"
  },
  single_file_support = true
}

require'lspconfig'.texlab.setup{
  cmd = {"texlab"},
  filetypes = {"tex", "plaintex", "bib"},
  single_file_support = true,
  settings = {
    texlab = {
      auxDirectory = ".",
      bibtexFormatter = "texlab",
      build = {
        executable = "pdflatex",
        forwardSearchAfter = true,
        onSave = true
      },
      chktex = {
        onEdit = true,
        onOpenAndSave = true
      },
      diagnosticsDelay = 100,
      formatterLineLength = 500,
      diagnostics = {
        ignoredPatterns = {".*may.*"}
      },
      forwardSearch = {
        executable = "zathura",
        args = {"--synctex-forward", "%l:1:%f", "%p"}
      },
      latexFormatter = "latexindent",
      latexindent = {
        modifyLineBreaks = false
      }
    }
  }
}

require'lspconfig'.clangd.setup {
    on_attach = function(client, bufnr)
        require("inlay-hints").on_attach(client, bufnr)
    end,
    flags = lsp_flags,
    cmd = { "clangd" },
    filetypes = { "c", "cpp" },
    single_file_support = true
  }

-- clangd_extensions

require("clangd_extensions").setup({
    ast = {
        -- These are unicode, should be available in any font
        role_icons = {
            type = "🄣",
            declaration = "🄓",
            expression = "🄔",
            statement = ";",
            specifier = "🄢",
            ["template argument"] = "🆃",
        },
        kind_icons = {
            Compound = "🄲",
            Recovery = "🅁",
            TranslationUnit = "🅄",
            PackExpansion = "🄿",
            TemplateTypeParm = "🅃",
            TemplateTemplateParm = "🅃",
            TemplateParamObject = "🅃",
        },
        --[[ These require codicons (https://github.com/microsoft/vscode-codicons)
            role_icons = {
                type = "",
                declaration = "",
                expression = "",
                specifier = "",
                statement = "",
                ["template argument"] = "",
            },

            kind_icons = {
                Compound = "",
                Recovery = "",
                TranslationUnit = "",
                PackExpansion = "",
                TemplateTypeParm = "",
                TemplateTemplateParm = "",
                TemplateParamObject = "",
            }, ]]

        highlights = {
            detail = "Comment",
        },
    },
    memory_usage = {
        border = "none",
    },
    symbol_info = {
        border = "none",
    },
})

EOF

autocmd FileType haskell lua vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', 'gd', vim.lsp.buf.definition, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', '<leader>h', vim.lsp.buf.hover, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', '<leader>f', vim.lsp.buf.format, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, {noremap=true, silent=true,buffer=bufnr})
autocmd FileType haskell lua vim.keymap.set('n', 'gr', vim.lsp.buf.references, {noremap=true, silent=true,buffer=bufnr})

lua << EOF

require('dressing').setup({
  input = {
    -- Set to false to disable the vim.ui.input implementation
    enabled = true,

    -- Default prompt string
    default_prompt = "Input:",

    -- Can be 'left', 'right', or 'center'
    prompt_align = "left",

    -- When true, <Esc> will close the modal
    insert_only = false,

    -- When true, input will start in insert mode.
    start_in_insert = false,

    -- These are passed to nvim_open_win
    -- anchor = "SW",
    border = "rounded",
    -- 'editor' and 'win' will default to being centered
    relative = "cursor",

    -- These can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
    prefer_width = 40,
    width = nil,
    -- min_width and max_width can be a list of mixed types.
    -- min_width = {20, 0.2} means "the greater of 20 columns or 20% of total"
    max_width = { 140, 0.9 },
    min_width = { 20, 0.2 },

    buf_options = {},
    win_options = {
      -- Window transparency (0-100)
      winblend = 10,
      -- Disable line wrapping
      wrap = false,
    },

    -- Set to `false` to disable
    mappings = {
      n = {
        ["<Esc>"] = "Close",
        ["<CR>"] = "Confirm",
      },
      i = {
        ["<C-c>"] = "Close",
        ["<CR>"] = "Confirm",
        ["<Up>"] = "HistoryPrev",
        ["<Down>"] = "HistoryNext",
      },
    },

    override = function(conf)
      -- This is the config that will be passed to nvim_open_win.
      -- Change values here to customize the layout
      return conf
    end,

    -- see :help dressing_get_config
    get_config = nil,
  },
  select = {
    -- Set to false to disable the vim.ui.select implementation
    enabled = true,

    -- Priority list of preferred vim.select implementations
    backend = { "telescope", "fzf_lua", "fzf", "builtin", "nui" },

    -- Trim trailing `:` from prompt
    trim_prompt = true,

    -- Options for telescope selector
    -- These are passed into the telescope picker directly. Can be used like:
    -- telescope = require('telescope.themes').get_ivy({...})
    telescope = nil,

    -- Options for fzf selector
    fzf = {
      window = {
        width = 0.5,
        height = 0.4,
      },
    },

    -- Options for fzf_lua selector
    fzf_lua = {
      winopts = {
        width = 0.5,
        height = 0.4,
      },
    },

    -- Options for nui Menu
    nui = {
      position = "50%",
      size = nil,
      relative = "editor",
      border = {
        style = "rounded",
      },
      buf_options = {
        swapfile = false,
        filetype = "DressingSelect",
      },
      win_options = {
        winblend = 10,
      },
      max_width = 80,
      max_height = 40,
      min_width = 40,
      min_height = 10,
    },

    -- Options for built-in selector
    builtin = {
      -- These are passed to nvim_open_win
      -- anchor = "NW",
      border = "rounded",
      -- 'editor' and 'win' will default to being centered
      relative = "editor",

      buf_options = {},
      win_options = {
        -- Window transparency (0-100)
        winblend = 10,
      },

      -- These can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
      -- the min_ and max_ options can be a list of mixed types.
      -- max_width = {140, 0.8} means "the lesser of 140 columns or 80% of total"
      width = nil,
      max_width = { 140, 0.8 },
      min_width = { 40, 0.2 },
      height = nil,
      max_height = 0.9,
      min_height = { 10, 0.2 },

      -- Set to `false` to disable
      mappings = {
        ["<Esc>"] = "Close",
        ["<C-c>"] = "Close",
        ["<CR>"] = "Confirm",
      },

      override = function(conf)
        -- This is the config that will be passed to nvim_open_win.
        -- Change values here to customize the layout
        return conf
      end,
    },

    -- Used to override format_item. See :help dressing-format
    format_item_override = {},

    -- see :help dressing_get_config
    get_config = nil,
  },
})

EOF

lua << EOF

require('nvim-silicon').setup({
  font = 'Mononoki Nerd Font Mono=32;Noto Color Emoji=32',
  to_clipboard = true,
  theme = 'Dracula',
  background = '#261e33',
  -- shadow_color = '#231c20',
  pad_horiz = 75,
  pad_vert = 60,
  line_pad = 2,
  gobble = true,
  no_window_controls = true,
  output = nil
  -- output = function()
	-- 	return "./" .. os.date("!%Y-%m-%d_%H-%M-%S") .. ".png"
	-- end,
})

EOF

""""""""""""""""""""""""""""""""""""""""""""""


filetype indent on
set number relativenumber    "Numbers on the left with absolute line number on current line
set wildmode=longest,list,full   "Command mode autocompletion
set splitbelow splitright    "Splits open below or on the right
"Auto-Center on enter insert:
autocmd InsertEnter * norm zz
syntax on
set path=.,,**
set ignorecase
set smartcase
set wrap
set foldmethod=marker
set viminfo
" source .exrc if exists in current working directory
set exrc
set secure
set tabstop=4
set shiftwidth=0
autocmd FileType java set tabstop=4
autocmd FileType java set shiftwidth=4
autocmd FileType groovy set tabstop=4
autocmd FileType groovy set shiftwidth=4
set expandtab
set nohlsearch
set nrformats=alpha,octal,hex,bin
autocmd FileType cpp set colorcolumn=80
autocmd FileType cpp highlight ColorColumn guibg=#2b2c33
autocmd FileType sh set colorcolumn=80
autocmd FileType sh highlight ColorColumn guibg=#2b2c33
autocmd FileType python set colorcolumn=80
autocmd FileType python highlight ColorColumn guibg=#2b2c33
autocmd FileType vim set nofoldenable
set updatetime=100

" conceal stuff
set concealcursor=""
set conceallevel=1
" but not in these filetypes
autocmd FileType json set conceallevel=0
autocmd FileType markdown set conceallevel=1
autocmd FileType make set conceallevel=0

" {{{
autocmd FileType cpp call matchadd("Conceal", '::', 10,-1, {'conceal': '∷'})
autocmd FileType cpp call matchadd("Conceal", '<<', 10,-1, {'conceal': '«'})
" autocmd FileType cpp call matchadd("Conceal", '>>', 10,-1, {'conceal': '»'})
autocmd FileType cpp call matchadd("Conceal", '->', 10,-1, {'conceal': '⇒'})
" autocmd FileType cpp call matchadd("Conceal", '<=>', 10,-1, {'conceal': '⟺'})
autocmd FileType cpp call matchadd("Conceal", '[]', 10,-1, {'conceal': 'λ'})
" autocmd FileType cpp call matchadd("Conceal", 'noexcept', 10,-1, {'conceal': '☓'})
autocmd FileType cpp call matchadd("Conceal", 'typename', 10,-1, {'conceal': '𝛵'})
" autocmd FileType cpp call matchadd("Conceal", 'return', 10,-1, {'conceal': '﬋'})
" autocmd FileType cpp call matchadd("Conceal", '\[\[nodiscard\]\]', 10,-1, {'conceal': '🯀'})
" autocmd FileType cpp call matchadd("Conceal", 'true', 10,-1, {'conceal': '⊤'})
" autocmd FileType cpp call matchadd("Conceal", 'false', 10,-1, {'conceal': '⊥'})
" autocmd FileType cpp call matchadd("Conceal", 'nullptr', 10,-1, {'conceal': ''})
" autocmd FileType cpp call matchadd("Conceal", 'class ', 10,-1, {'conceal': '§'})
" autocmd FileType cpp call matchadd("Conceal", 'struct ', 10,-1, {'conceal': '§'})
" autocmd FileType cpp call matchadd("Conceal", 'case ', 10,-1, {'conceal': '▸'})
" autocmd FileType cpp call matchadd("Conceal", 'break;', 10,-1, {'conceal': '◁'})

autocmd FileType java call matchadd("Conceal", 'private', 10,-1, {'conceal': '■'})
autocmd FileType java call matchadd("Conceal", 'protected', 10,-1, {'conceal': '◆'})
autocmd FileType java call matchadd("Conceal", 'public', 10,-1, {'conceal': '▲'})
" autocmd FileType java call matchadd("Conceal", 'return', 10,-1, {'conceal': '﬋'})
" autocmd FileType java call matchadd("Conceal", 'class ', 10,-1, {'conceal': '§'})
" autocmd FileType java call matchadd("Conceal", 'true', 10,-1, {'conceal': '⊤'})
" autocmd FileType java call matchadd("Conceal", 'false', 10,-1, {'conceal': '⊥'})

call matchadd("Conceal", '+-', 10,-1, {'conceal': '±'})
call matchadd("Conceal", '!=', 10,-1, {'conceal': '≠'})
call matchadd("Conceal", '>=', 10,-1, {'conceal': '≥'})
call matchadd("Conceal", '<=', 10,-1, {'conceal': '≤'})
" }}}

"Better visual block indenting
vnoremap < <gv
vnoremap > >gv

" Y behavior
nnoremap Y y$

" keep cursor centered when searching
nnoremap n nzzzv
nnoremap N Nzzzv

"Quicker split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" tab stuff
nnoremap <Tab> gt
nnoremap <S-Tab> gT
" nnoremap <A-t> :tabnew<space>%<CR>
nmap <A-t> :tabnew<CR>
nmap g<A-t> mm:tabnew %<CR>'m


" inoremap ;; <esc>A;<esc>
inoremap ;\ <esc>/<++><CR>"_d4l<esc>:nohl<CR>i

" Markdown stuff
autocmd FileType markdown nnoremap <buffer> <leader>p :w<CR>:AsyncRun mdpdf %<CR>
autocmd FileType markdown nnoremap <buffer> <leader>0p i![]()<left><C-r>=system('snapshot.sh /dev/video0')<CR><esc>F]i
autocmd FileType markdown nnoremap <buffer> <leader>1p i![]()<left><C-r>=system('snapshot.sh /dev/video1')<CR><esc>F]i
autocmd FileType markdown nnoremap <buffer> <leader>2p i![]()<left><C-r>=system('snapshot.sh /dev/video2')<CR><esc>F]i
autocmd FileType markdown nnoremap <buffer> <leader>3p i![]()<left><C-r>=system('snapshot.sh /dev/video3')<CR><esc>F]i
autocmd FileType markdown setlocal wrap
autocmd FileType markdown setlocal linebreak
autocmd FileType markdown,text setlocal spell
autocmd FileType markdown setlocal spelllang=en_us
" autocmd InsertLeave,TextChanged *.md :write

" Latex stuff
function! CompileTex()
  write
  AsyncRun pdflatex -synctex 1 %
endfunction
autocmd FileType tex,plaintex nnoremap <buffer> <leader>p :call CompileTex()<CR>
autocmd FileType tex,plaintex nnoremap <buffer> <leader><leader>p :!pdflatex -synctex 1 %<CR>
autocmd FileType tex,plaintex nnoremap <buffer> \w "wdiWi\begin{<C-r>w}<CR><CR>\end{<C-r>w}<up>
autocmd FileType tex,plaintex nnoremap <buffer> \lr "wyiwi\left<esc>la<space><space>\right<C-r>w<esc>Bhi
autocmd FileType tex,plaintex setlocal wrap
autocmd FileType tex,plaintex setlocal linebreak
autocmd FileType tex,plaintex setlocal spell
autocmd FileType tex,plaintex setlocal spelllang=en_us
" autocmd InsertLeave,TextChanged *.tex :write

" Thank you StackExchange! https://vi.stackexchange.com/questions/4120/how-to-enable-disable-an-augroup-on-the-fly#4123
" function! ToggleTexAutoCompile()
"   if !exists('#TexAutoCompile#InsertLeave')
"     augroup TexAutoCompile
"         autocmd!
"         autocmd InsertLeave,TextChanged <buffer> :call CompileTex()
"     augroup END
"     echo "Enabled auto-compile"
"   else
"     augroup TexAutoCompile
"       autocmd!
"     augroup END
"     echo "Disabled auto-compile"
"   endif
" endfunction
" autocmd FileType tex nnoremap <buffer> <F4> :call ToggleTexAutoCompile()<CR>

" Graphviz stuff
autocmd FileType dot nnoremap <buffer> <leader>p :w<CR>:AsyncRun dotpng %<CR>

" PlantUML stuff
autocmd FileType plantuml nnoremap <buffer> <leader>p :w<CR>:AsyncRun plantuml -failfast2 %<CR>

" C++ stuff
autocmd FileType cpp inoremap ;ol std::cout <<  << '\n';<esc>02f<la
autocmd FileType cpp inoremap ;ot std::cout <<  << '\t';<esc>02f<la
autocmd FileType cpp inoremap //{ /* {{{ */<CR>/* }}} */<esc>k$F{a<space>
autocmd FileType cpp inoremap ;d /* {{{ */<CR>/* }}} */<esc>k$F{a<space>doc<esc>o/**<CR><CR><BS>/<up><space>
autocmd FileType cpp vnoremap Sz o<esc>O/* {{{ */<esc>gvo<esc>o/* }}} */<esc>F}%$F{a<space>
autocmd FileType cpp nnoremap \t :AsyncRun tag<CR>
autocmd FileType cpp nnoremap <leader>i :vs<CR>:ClangdSwitchSourceHeader<CR>
autocmd FileType cpp nnoremap <leader>ci :ClangdSymbolInfo<CR>
autocmd FileType cpp nnoremap <leader>ch :ClangdTypeHierarchy<CR>
" jank workaround to misconfiguration of nvim-autopairs
autocmd FileType cpp inoremap << <<

" Python stuff
autocmd FileType python vnoremap Sz o<esc>O# {{{<space><esc><<gvo<esc>o# }}}<esc>F}%$F{la

" Java stuff
autocmd FileType java inoremap ;ol System.out.println();<left><left>
autocmd FileType java inoremap ;n <esc>^yiWA = new <C-r>"();<left><left>
autocmd FileType java vnoremap Sz o<esc>O//{{{<esc>gvo<esc>o//}}}<esc>F}%$F{>>A<space>
autocmd FileType java nnoremap \t :AsyncRun jtag<CR>
autocmd FileType java inoremap ;d /**<CR><CR><BS>/<up><space>
" jdtls
autocmd FileType java nnoremap <A-o> <Cmd>lua require'jdtls'.organize_imports()<CR>
autocmd FileType java nnoremap crv <Cmd>lua require('jdtls').extract_variable()<CR>
autocmd FileType java vnoremap crv <Esc><Cmd>lua require('jdtls').extract_variable(true)<CR>
autocmd FileType java nnoremap crc <Cmd>lua require('jdtls').extract_constant()<CR>
autocmd FileType java vnoremap crc <Esc><Cmd>lua require('jdtls').extract_constant(true)<CR>
autocmd FileType java vnoremap crm <Esc><Cmd>lua require('jdtls').extract_method(true)<CR>

autocmd FileType java nnoremap <silent> <buffer> gd         :lua vim.lsp.buf.definition()<CR>
autocmd FileType java nnoremap <silent> <buffer> <leader>h  :lua vim.lsp.buf.hover()<CR>
autocmd FileType java nnoremap <silent> <buffer> gi         :lua vim.lsp.buf.implementation()<CR>
autocmd FileType java nnoremap <silent> <buffer> <leader>D  :lua vim.lsp.buf.type_definition()<CR>
autocmd FileType java nnoremap <silent> <buffer> <leader>rn :lua vim.lsp.buf.rename()<CR>
autocmd FileType java nnoremap <silent> <buffer> <leader>ca :lua vim.lsp.buf.code_action()<CR>
autocmd FileType java nnoremap <silent> <buffer> gr         :lua vim.lsp.buf.references()<CR>

nnoremap K kJ
nnoremap gK K
nnoremap ' `
nnoremap ` '
nnoremap g<C-]> mt:tabnew<space>%<CR>`t<C-]>

nnoremap <leader>q :q<CR>
nnoremap <leader><leader>q :q!<CR>
nnoremap <leader>w :w<CR>
nnoremap <leader><leader>w :wa<CR>
nnoremap <leader>d :bd<CR>
" nnoremap <leader>e :vs<CR>:e<space>.<CR>
nnoremap <leader>v :vs<CR>
nnoremap <leader>s :sp<CR>
nnoremap ;\ /<++><CR>"_d4l<esc>:nohl<CR>i

nnoremap g<space> i<space><esc>la<space><esc>
nnoremap go o<CR><esc>O
nnoremap gO O<CR><esc>O

cnoremap :h tabnew<CR>:h<space>help<CR><C-w><C-k>:q<CR>:h<space>
cnoremap :vh vert<space>help<space>
cnoremap :Man tabnew<CR>:Man<space>man<CR><C-w><C-k>:q<CR>:Man<space>

" highlightedyank (for some reason this doesn't work unless at the very end)
let g:highlightedyank_highlight_duration = 800
" highlight HighlightedyankRegion ctermbg=237 guibg=#9b6bdf guifg='White'
highlight HighlightedyankRegion guibg=#9b6bdf guifg='White'

" prettier search highlighting
highlight Search guibg=#9b6bdf guifg='White'

highlight Conceal guifg=#bd93f9 guibg=NONE

highlight TODO guifg=#9876aa guibg=NONE
highlight FIXME guifg=#9876aa guibg=NONE

norm :<esc>
